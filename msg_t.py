#Message Type

class msgType():
    def __init__(self):
        self.bufferString = ""
        self.bufferBinary = b""
        self.binaryMode = False
        self.dataArrived = False
    
    def clear(self):
        self.dataArrived = False
        self.bufferString = ""
        self.bufferBinary = b""