#python Serial

import threading
import serial
import time

from msg_t import msgType

#Global variables
ser = serial.Serial()
msg = msgType()
msg.binaryMode = True

class SerialController():
    def __init__(self):
        self.initWorker()
        self.initSerial()

    def initWorker(self):
        myWorker = WorkerThread()
        myWorker.start()

    def initSerial(self):
        ser.baudrate = 115200
        ser.port = "COM15"
        ser.open()

        if ser.isOpen():
            print("Serial opened")
            mySerial = SerialThread()
            mySerial.start()
        else :
            print("Failed to open and start Serial")
    
# Thread Class
class SerialThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        
    def run (self):
        global ser
        global msg
        while True:
            if ser.isOpen():
                if(msg.binaryMode):
                    dataBinary = ser.read()
                    # \n is the end character in this protocol, you can choose anything else
                    if(dataBinary != b'\n'):
                        msg.bufferBinary += dataBinary
                    else:
                        msg.dataArrived = True

                else :
                    dataString = ser.readline().decode('unicode_escape')
                    msg.bufferString = dataString
                    msg.dataArrived = True

class WorkerThread (threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
    
    def run(self):
        global msg
        counter = 0

        while True:
            print("Cntr:" + str(counter))
            counter += 1

            if(msg.dataArrived):
                if msg.binaryMode :
                    print("Bynary data arrived: ", end ="")
                    print(msg.bufferBinary)
                    msg.clear()
                    time.sleep(1)
                else :
                    print("Message: " + msg.bufferString)
                    msg.clear()
                    time.sleep(1)
            else :
                time.sleep(1)